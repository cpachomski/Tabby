import React from "react";
import { ActionCreators } from "redux-undo";
import { connect } from "react-redux";
import IconButton from "components/icon-button";

const TimeMachine = ({ canUndo, canRedo, onUndo, onRedo }) => {
  return (
    <div>
      <IconButton glyph="undo" onClick={onUndo} disabled={!canUndo} />
      <IconButton glyph="redo" onClick={onRedo} disabled={!canRedo} />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    canUndo: state.past.length > 0,
    canRedo: state.future.length > 0
  };
};

const mapDispatchtoProps = dispatch => {
  return {
    onUndo: () => dispatch(ActionCreators.undo()),
    onRedo: () => dispatch(ActionCreators.redo())
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(TimeMachine);
