import React, { Component } from "react";
import { connect } from "react-redux";
import {
  REMOVE_MEASURE,
  SET_MEASURE_TITLE,
  ADD_STRING,
  REMOVE_STRING
} from "actions/measure";
import { ADD_CHORD, REMOVE_LAST_CHORD } from "actions/chord";
import Chord from "components/chord";
import IconButton from "components/icon-button";
import ControlBar from "components/control-bar";
import styles from "./styles.css";

class Measure extends Component {
  constructor(props) {
    super(props);

    this.updateTitle = this.updateTitle.bind(this);
  }

  updateTitle(e) {
    const { measureId, dispatch } = this.props;
    const name = e.target.value;

    dispatch({ type: SET_MEASURE_TITLE, measureId, name });
  }

  render() {
    const { chords, measureId, tunings, name, dispatch } = this.props;

    const lastChordIndex = chords.length - 1;

    return (
      <div className={styles.measure}>
        <div className={styles.title}>
          <input
            value={name}
            placeholder="Section Name"
            type="text"
            onChange={this.updateTitle}>
            {name}
          </input>
        </div>
        <div className={styles.chords}>
          {chords.map((chord, i) => {
            return (
              <Chord
                {...chord}
                chordId={i}
                measureId={measureId}
                tunings={tunings}
              />
            );
          })}
        </div>
        <ControlBar>
          <IconButton
            glyph="arrow_downward"
            onClick={() => dispatch({ type: ADD_STRING, measureId })}
          />
          <IconButton
            glyph="arrow_upward"
            onClick={() => dispatch({ type: REMOVE_STRING, measureId })}
          />
          <IconButton
            glyph="subdirectory_arrow_left"
            onClick={() => {
              dispatch({
                type: REMOVE_LAST_CHORD,
                chordId: lastChordIndex,
                measureId
              });
            }}
          />
          <IconButton
            glyph="subdirectory_arrow_right"
            onClick={() => dispatch({ type: ADD_CHORD, measureId })}
          />
          <IconButton
            glyph="delete"
            onClick={() =>
              dispatch({
                type: REMOVE_MEASURE,
                measureId
              })
            }
          />
        </ControlBar>
      </div>
    );
  }
}

export default connect()(Measure);
