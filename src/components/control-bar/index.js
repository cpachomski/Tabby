import React, { Component } from "react";
import styles from "./styles.css";

class DropDown extends Component {
	render() {
		return (
			<div className={styles.controlBar}>
				<ul className={styles.menu}>
					{React.Children.map(this.props.children, child => {
						const Child = React.cloneElement(child);
						return (
							<li className={styles.menuItem}>
								{Child}
							</li>
						);
					})}
				</ul>
			</div>
		);
	}
}

export default DropDown;
