import React from "react";
import { SET_TUNING } from "actions/tuning";
import { connect } from "react-redux";
import styles from "./styles.css";

const Tuning = ({ tuning, measureId, tuningId, dispatch }) => {
  return (
    <input
      className={styles.tuning}
      value={tuning}
      maxlength={2}
      onChange={e =>
        dispatch({
          type: SET_TUNING,
          measureId,
          tuningId,
          tuning: e.target.value
        })}
    />
  );
};

export default connect()(Tuning);
