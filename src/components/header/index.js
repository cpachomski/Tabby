import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_MEASURE } from "actions/measure";
import IconButton from "components/icon-button";
import ShareBox from "components/share-box";
import TimeMachine from "components/time-machine";
import styles from "./styles.css";

class Header extends Component {
  constructor() {
    super();
    this.state = {
      shareVisible: false
    };

    this.toggleShare = this.toggleShare.bind(this);
  }

  toggleShare() {
    this.setState({
      shareVisible: !this.state.shareVisible
    });
  }

  render() {
    const { dispatch } = this.props;
    const { shareVisible } = this.state;

    return (
      <div className={styles.header}>
        <div className={styles.container}>
          <h1>Tabbr</h1>
          <div className={styles.controls}>
            <TimeMachine />
            <IconButton glyph="playlist_add" onClick={() => dispatch({ type: ADD_MEASURE })} />
            <IconButton glyph="share" onClick={this.toggleShare} />
            <IconButton glyph="info" onClick={() => alert("test")} />
            <ShareBox active={shareVisible} clickHandler={this.toggleShare} />
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Header);
