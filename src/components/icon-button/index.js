import React from "react";
import styles from "./styles.css";

const IconButton = ({ glyph, onClick, disabled, hintText }) => {
  return (
    <button
      className={`${styles.iconButton} material-icons`}
      data-hint={hintText}
      onClick={e => {
        onClick();
        e.stopPropagation();
      }}
      disabled={disabled}>
      {glyph}
    </button>
  );
};

export default IconButton;
