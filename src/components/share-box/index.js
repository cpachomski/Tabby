import React from "react";
import { connect } from "react-redux";
import { SET_SHEET_STATE } from "actions/sheet";
import IconButton from "components/icon-button";
import SocialShare from "components/social-share";
import styles from "./styles.css";

const StateInput = ({ dispatch, state, active, clickHandler }) => {
  const shareUrl = `${window.origin}?state=${btoa(JSON.stringify(state))}`;
  return (
    <div className={`${styles.stateInput} ${active ? styles.active : ""}`}>
      <div className={styles.header}>
        <h3>Share</h3>
        <IconButton glyph="close" onClick={clickHandler} />
      </div>
      <label className={styles.label} for="shareUrl">
        Share link
      </label>
      <p id="shareUrl" className={styles.shareUrl}>
        {shareUrl}
      </p>
      <SocialShare shareUrl={shareUrl} />
    </div>
  );
};

const mapStateToProps = ({ present }) => ({ state: present });

export default connect(mapStateToProps)(StateInput);
