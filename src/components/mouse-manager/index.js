import React, { Component } from "react";
import { connect } from "react-redux";
import { SET_MOUSE_UP, SET_MOUSE_DOWN } from "actions/mouse";
import { SET_FRAME_TO_CLIPBOARD } from "actions/clipboard";

class MouseManager extends Component {
  componentDidMount() {
    const { dispatch, chordHovered } = this.props;
    document.body.onmousedown = () => {
      dispatch({ type: SET_MOUSE_DOWN });
    };

    document.body.onmouseup = () => {
      dispatch({ type: SET_MOUSE_UP });
      dispatch({ type: SET_FRAME_TO_CLIPBOARD });
    };
  }

  render() {
    return null;
  }
}

const mapStateToProps = ({ chordHovered }) => ({ chordHovered });

export default connect(mapStateToProps)(MouseManager);
