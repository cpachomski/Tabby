import React, { Component } from "react";
import fingers from "./fingers";
import styles from "./styles.css";

class NoteSelector extends Component {
  constructor() {
    super();

    this.state = {};
  }

  render() {
    const { setFinger, beneath, active } = this.props;
    const activeClass = active ? styles.active : "";
    const beneathClass = beneath ? styles.beneath : "";

    return (
      <ul className={`${styles.noteSelector} ${activeClass} ${beneathClass}`}>
        <li onClick={e => setFinger("", e)}>Clear</li>
        {fingers.map(finger =>
          <li onClick={e => setFinger(finger, e)} data-finger={finger}>
            <span />
          </li>
        )}
      </ul>
    );
  }
}

export default NoteSelector;
