import React, { Component } from "react";
import { connect } from "react-redux";
import { SET_SHEET_NAME } from "actions/sheet";
import TimeMachine from "components/time-machine";
import styles from "./styles.css";

let keys = {
  cmd: false,
  m: false
};

const SheetHeader = ({ dispatch, sheetName }) => (
  <div ref="sheetHeader" className={styles.sheetHeader}>
    <h2>
      <input
        type="text"
        className={styles.sheetName}
        value={sheetName}
        onChange={e =>
          dispatch({
            type: SET_SHEET_NAME,
            sheetName: e.target.value
          })
        }
        placeholder="Song Name"
      />
    </h2>
  </div>
);

const mapStateToProps = ({ present }) => {
  return {
    sheetName: present.sheetName
  };
};

export default connect(mapStateToProps)(SheetHeader);
