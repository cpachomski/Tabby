import React from "react";
import styles from "./styles.css";

const SocialShare = ({ shareUrl }) => {
  const twitterIntentUrl = encodeURI(
    `https://twitter.com/intent/tweet?url=${shareUrl}&short_url_length=32&short_url_length_https=23`
  );

  const facebookIntentUrl = encodeURI(
    `https://www.facebook.com/dialog/share?u=${shareUrl}&display=popup`
  );
  return (
    <div className={styles.socialShare}>
      <a href={``} target="_blank" rel="noopener">
        <i className="fa fa-facebook" />
      </a>
      <a href={twitterIntentUrl} target="_blank" rel="noopener">
        <i className="fa fa-twitter" />
      </a>
    </div>
  );
};

export default SocialShare;
