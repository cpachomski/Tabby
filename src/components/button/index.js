import React from "react";
import styles from "./styles.css";

const Button = ({ type, text }) => {
  return <button className="base ${type}">{text}</button>;
};

export default Button;
