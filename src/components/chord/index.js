import React, { Component } from "react";
import { CLEAR_CHORD, SET_CHORD_HOVERED } from "actions/chord";
import {
  SET_CLIPBOARD_CHORD,
  SET_SELECTION_FRAME,
  SET_SELECTION_FRAME_START,
  SET_FRAME_TO_CLIPBOARD,
  PASTE_CLIPBOARD_TO_MEASURE
} from "actions/clipboard";
import { connect } from "react-redux";
import Note from "components/note";
import Tuning from "components/tuning";
import styles from "./styles.css";

class Chord extends Component {
  constructor() {
    super();

    this.state = {
      showTunings: false,
      isPressed: false
    };

    this.setListeners = this.setListeners.bind(this);
    this.unsetListeners = this.unsetListeners.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.checkAndAddToClipboard = this.checkAndAddToClipboard.bind(this);
  }

  componentDidMount() {
    const { chord } = this.refs;
    const { previousSibling } = chord;

    if (!previousSibling) {
      return this.setState({
        showTunings: true
      });
    }

    const currentRight = chord.getBoundingClientRect().right;
    const previousRight = previousSibling.getBoundingClientRect().right;

    if (currentRight < previousRight) {
      return this.setState({
        showTunings: true
      });
    }
  }

  checkAndAddToClipboard() {
    const { mousedown, dispatch, chordId, measureId } = this.props;
    if (!mousedown) return;
    dispatch({ type: SET_SELECTION_FRAME, chordId, measureId });
  }

  setListeners() {
    window.addEventListener("keydown", this.handleKeyDown);
    window.addEventListener("keyup", this.handleKeyUp);
    this.checkAndAddToClipboard();
  }

  unsetListeners() {
    window.removeEventListener("keydown", this.handleKeyDown);
    window.removeEventListener("keyup", this.handleKeyUp);
  }

  handleKeyDown(e) {
    const keyPressActions = {
      "67": SET_FRAME_TO_CLIPBOARD, // "C" -> COPY
      "86": PASTE_CLIPBOARD_TO_MEASURE, // "V" -> PASTE
      "68": CLEAR_CHORD // "D" -> DELETE
    };
    const keyCode = e.keyCode.toString();
    const { dispatch, measureId, chordId } = this.props;
    if (keyPressActions[keyCode]) {
      if (keyPressActions[keyCode] === SET_FRAME_TO_CLIPBOARD) {
        this.setState({ isPressed: true });
        dispatch({ type: SET_SELECTION_FRAME, measureId, chordId });
      }

      dispatch({
        type: keyPressActions[keyCode],
        measureId,
        chordId
      });
    }
  }

  handleKeyUp(e) {
    this.setState({ isPressed: false });
  }

  isSelected(selectionFrame, measureId, chordId) {
    return (
      measureId == selectionFrame.measureId &&
      (chordId <= selectionFrame.end && chordId >= selectionFrame.start)
    );
  }

  render() {
    const {
      chordId,
      measureId,
      selectionFrame,
      notes,
      tunings,
      dispatch
    } = this.props;
    const { showTunings, isPressed } = this.state;

    const selectedClass =
      this.isSelected(selectionFrame, measureId, chordId) || isPressed
        ? styles.selected
        : "";

    return (
      <div
        ref="chord"
        className={`${styles.chord} ${selectedClass}`}
        onMouseEnter={() => {
          this.setListeners();
          dispatch({ type: SET_CHORD_HOVERED, isHovered: true });
        }}
        onMouseLeave={() => {
          this.unsetListeners();
          this.setState({ isPressed: false });
          dispatch({ type: SET_CHORD_HOVERED, isHovered: false });
        }}
        onMouseUp={() => {
          this.setState({ isPressed: false });
        }}
        onMouseDown={() => {
          this.setState({ isPressed: true });
          dispatch({ type: SET_SELECTION_FRAME_START, measureId, chordId });
        }}>
        {showTunings && (
          <div className={styles.tunings}>
            {tunings.map((tuning, tuningId) => {
              return (
                <Tuning
                  tuningId={tuningId}
                  tuning={tuning}
                  measureId={measureId}
                />
              );
            })}
          </div>
        )}
        {notes.map((note, i) => (
          <Note
            unsetListeners={this.unsetListeners}
            {...note}
            chordId={chordId}
            measureId={measureId}
            noteId={i}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = ({ present }) => ({
  mousedown: present.clipboard.mousedown,
  selectionFrame: present.clipboard.selectionFrame
});

export default connect(mapStateToProps)(Chord);
