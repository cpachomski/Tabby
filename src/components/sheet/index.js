import React, { Component } from "react";
import { connect } from "react-redux";
import { SET_SHEET_NAME } from "actions/sheet";
import SheetHeader from "components/sheet-header";
import Measure from "components/measure";
import styles from "./styles.css";

class Sheet extends Component {
  render() {
    const { measures, sheetName, dispatch } = this.props;
    return (
      <div className={styles.sheet}>
        <SheetHeader />
        {measures.map((measure, idx) => {
          return <Measure measureId={idx} {...measure} />;
        })}
      </div>
    );
  }
}

const mapStateToProps = ({ present }) => {
  return {
    measures: present.measures,
    sheetName: present.sheetName
  };
};

export default connect(mapStateToProps)(Sheet);
