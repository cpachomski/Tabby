import React, { Component } from "react";
import { connect } from "react-redux";
import NoteSelector from "components/note-selector";
import { SET_NOTE } from "actions/note";
import styles from "./styles.css";

const keyCodeFingerMap = {
  "48": 0,
  "49": 1,
  "50": 2,
  "51": 3,
  "52": 4
};

class Note extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      showNotePicker: false
    };

    this.toggleVisible = this.toggleVisible.bind(this);
    this.toggleNotePicker = this.toggleNotePicker.bind(this);
    this.setFinger = this.setFinger.bind(this);
  }

  toggleVisible() {
    const { isVisible } = this.state;

    if (isVisible) {
      // reset to 0 via redux
      this.setState({ isVisible: false });
    } else {
      this.setState({ isVisible: true });
    }
  }

  toggleNotePicker() {
    this.setState({
      showNotePicker: true
    });
  }

  setFinger(finger, e) {
    e.stopPropagation();

    const { dispatch, measureId, chordId, noteId, unsetListeners } = this.props;

    this.setState({
      showNotePicker: false
    });

    unsetListeners();
    dispatch({ type: SET_NOTE, measureId, chordId, noteId, finger });
  }

  render() {
    const { finger } = this.props;
    const { showNotePicker } = this.state;
    const fingerClass = finger || finger === 0 ? styles.isVisible : null;

    return (
      <div
        className={`${styles.note} ${showNotePicker
          ? styles.showNotePicker
          : ""}`}
        onClick={this.toggleNotePicker}
        onMouseEnter={this.addKeyboardListener}
        onMouseLeave={this.removeKeyboardListener}
      >
        <span
          className={`${fingerClass} ${finger === "+" ? styles.xClass : ""}`}
        >
          {finger}
        </span>
        {showNotePicker && (
          <NoteSelector setFinger={this.setFinger} active={showNotePicker} />
        )}
      </div>
    );
  }
}

export default connect()(Note);
