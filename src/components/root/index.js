import React, { Component } from "react";
import { Provider } from "react-redux";
import configureStore from "store";
import Header from "components/header";
import MouseManager from "components/mouse-manager";
import Sheet from "components/sheet";
import styles from "./styles.css";
const store = configureStore();

class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className={styles.root}>
          <Header />
          <Sheet />
          <MouseManager />
        </div>
      </Provider>
    );
  }
}

export default Root;
