import "react-hot-loader/patch";
import React from "react";
import ReactDOM from "react-dom";
import Root from "components/root";

const render = Component => {
	ReactDOM.render(<Component />, document.getElementById("tabbr"));
};

render(Root);

if (module.hot) {
	module.hot.accept("./components/root", () => {
		render(Root);
	});
}
