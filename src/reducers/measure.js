import update from "immutability-helper";
import { initialMeasure } from "store/defaults";
import {
  ADD_MEASURE,
  REMOVE_MEASURE,
  SET_MEASURE_TITLE,
  ADD_STRING,
  REMOVE_STRING
} from "actions/measure";

export default {
  [ADD_MEASURE]: (state, action) => {
    return update(state, { measures: { $push: [initialMeasure] } });
  },
  [REMOVE_MEASURE]: (state, action) => {
    let { measureId } = action;
    if (action.measureId == undefined) {
      const { measures } = state;
      measureId = measures.length - 1;
    }
    return update(state, {
      measures: { $splice: [[measureId, 1]] }
    });
  },
  [SET_MEASURE_TITLE]: (state, action) => {
    const { measureId, name } = action;
    return update(state, {
      measures: {
        [measureId]: { $merge: { name } }
      }
    });
  },
  [ADD_STRING]: (state, action) => {
    const { tunings, chords } = state.measures[action.measureId];

    const newChords = chords.map(chord => {
      return {
        notes: chord.notes.concat({})
      };
    });

    return update(state, {
      measures: {
        [action.measureId]: {
          tunings: { $push: ["E"] },
          chords: { $set: newChords }
        }
      }
    });
  },
  [REMOVE_STRING]: (state, action) => {
    const { tunings, chords } = state.measures[action.measureId];
    let notes;
    const newChords = chords.map(chord => {
      let notes = chord.notes;

      return {
        notes: notes.slice(0, notes.length - 1)
      };
    });

    return update(state, {
      measures: {
        [action.measureId]: {
          tunings: { $splice: [[tunings.length - 1, 1]] },
          chords: { $set: newChords }
        }
      }
    });
  },
  undoable: [
    ADD_MEASURE,
    REMOVE_MEASURE,
    SET_MEASURE_TITLE,
    ADD_STRING,
    REMOVE_STRING
  ]
};
