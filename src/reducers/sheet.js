import update from "immutability-helper";
import { SET_SHEET_NAME, SET_SHEET_STATE } from "actions/sheet";

export default {
  [SET_SHEET_NAME]: (state, action) => {
    return update(state, {
      sheetName: { $set: action.sheetName }
    });
  },
  [SET_SHEET_STATE]: (state, action) => {
    return update(state, { $set: JSON.parse(action.sheetState) });
  },
  undoable: [SET_SHEET_NAME, SET_SHEET_STATE]
};
