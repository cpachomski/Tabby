import update from "immutability-helper";
import { emptyChord } from "store/defaults";
import {
  ADD_CHORD,
  REMOVE_LAST_CHORD,
  CLEAR_CHORD,
  SET_CHORD_HOVERED
} from "actions/chord";

export default {
  [ADD_CHORD]: (state, action) => {
    const { notes } = state.measures[action.measureId].chords[0];
    const newEmptyChord = notes.map(note => {
      return {};
    });

    return update(state, {
      measures: {
        [action.measureId]: {
          chords: { $push: [{ notes: newEmptyChord }] }
        }
      }
    });
  },
  [REMOVE_LAST_CHORD]: (state, action) => {
    if (state.measures[action.measureId].chords.length < 2) {
      return state;
    }

    return update(state, {
      measures: {
        [action.measureId]: { chords: { $splice: [[action.chordId, 1]] } }
      }
    });
  },
  [CLEAR_CHORD]: (state, action) => {
    return update(state, {
      measures: {
        [action.measureId]: {
          chords: {
            [action.chordId]: { $set: emptyChord }
          }
        }
      }
    });
  },
  [SET_CHORD_HOVERED]: (state, action) => {
    return update(state, {
      clipboard: { chordHovered: { $set: action.isHovered } }
    });
  },
  undoable: [ADD_CHORD, REMOVE_LAST_CHORD, CLEAR_CHORD]
};
