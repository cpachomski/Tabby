import update from "immutability-helper";
import { SET_TUNING } from "actions/tuning";

export default {
  [SET_TUNING]: (state, action) => {
    return update(state, {
      measures: {
        [action.measureId]: {
          tunings: {
            [action.tuningId]: { $set: action.tuning }
          }
        }
      }
    });
  },
  undoable: [SET_TUNING]
};
