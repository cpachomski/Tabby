import update from "immutability-helper";
import { SET_NOTE } from "actions/note";

export default {
  [SET_NOTE]: (state, action) => {
    const { measureId, chordId, noteId, finger } = action;
    return update(state, {
      measures: {
        [measureId]: {
          chords: {
            [chordId]: {
              notes: {
                [noteId]: { $set: { finger } }
              }
            }
          }
        }
      }
    });
  },
  undoable: [SET_NOTE]
};
