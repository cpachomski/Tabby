import update from "immutability-helper";
import { SET_MOUSE_UP, SET_MOUSE_DOWN } from "actions/mouse";

export default {
  [SET_MOUSE_UP]: (state, action) => {
    return update(state, {
      clipboard: {
        mousedown: { $set: false }
      }
    });
  },
  [SET_MOUSE_DOWN]: (state, action) => {
    return update(state, {
      clipboard: {
        mousedown: { $set: true }
      }
    });
  }
};
