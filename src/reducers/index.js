import undoable, { includeAction } from "redux-undo";
import { initialState } from "store/defaults";

const reducerImports = [
  require("./chord").default,
  require("./clipboard").default,
  require("./measure").default,
  require("./mouse").default,
  require("./note").default,
  require("./sheet").default,
  require("./tuning").default
];

let reducers = {};
let undoables = [];
reducerImports.map(reducer => {
  undoables = undoables.concat(reducer.undoable);
  Object.assign(reducers, reducer);
});

const getQueryParam = paramKey => {
  return (window.location.search.match(
    new RegExp(`[?&]${paramKey}=([^&]+)`)
  ) || [null])[1];
};

let INITIAL_STATE = initialState;

const stateParam = getQueryParam("state");
if (stateParam) {
  INITIAL_STATE = JSON.parse(atob(stateParam));
}

const rootReducer = (state = INITIAL_STATE, action) => {
  if (!reducers[action.type]) return state;
  return reducers[action.type](state, action);
};

const undoableRootReducer = undoable(rootReducer, {
  filter: includeAction(undoables)
});

export default undoableRootReducer;
