import update from "immutability-helper";
import { dummyFrame } from "store/defaults";
import {
  SET_CLIPBOARD_CHORD,
  PASTE_CLIPBOARD_CHORD,
  SET_SELECTION_FRAME_START,
  SET_SELECTION_FRAME,
  SET_FRAME_TO_CLIPBOARD,
  PASTE_CLIPBOARD_TO_MEASURE
} from "actions/clipboard";

export default {
  [SET_CLIPBOARD_CHORD]: (state, action) => {
    const { measureId, chordId } = action;
    return update(state, {
      clipboard: { chord: { $set: { measureId, chordId } } }
    });
  },
  [PASTE_CLIPBOARD_CHORD]: (state, action) => {
    const { chord } = state.clipboard;
    const { measureId, chordId } = action;
    const clipboardChord =
      state.measures[chord.measureId].chords[chord.chordId];

    return update(state, {
      measures: {
        [measureId]: {
          chords: {
            [chordId]: { $set: clipboardChord }
          }
        }
      }
    });
  },
  [PASTE_CLIPBOARD_TO_MEASURE]: (state, action) => {
    const { clipboard, measures } = state;
    console.log(measures[clipboard.pastableFrame.measureId]);
    const frame = measures[clipboard.pastableFrame.measureId].chords.filter(
      (c, i) =>
        i >= clipboard.pastableFrame.start && i <= clipboard.pastableFrame.end
    );
    console.log(frame);

    return update(state, {
      measures: {
        [action.measureId]: {
          chords: { $splice: [[action.chordId, frame.length, ...frame]] }
        }
      }
    });
  },
  [SET_SELECTION_FRAME_START]: (state, action) => {
    if (!state.chordHovered) return state;
    const { measureId, chordId } = action;

    return update(state, {
      clipboard: {
        selectionFrame: {
          measureId: { $set: measureId },
          prevChordId: { $set: chordId },
          start: { $set: chordId },
          end: { $set: null }
        }
      }
    });
  },
  [SET_SELECTION_FRAME]: (state, action) => {
    const { chordId, measureId, isSingleNote } = action;
    const { start, end, prevChordId } = state.clipboard.selectionFrame;
    const selectionMeasureId = state.clipboard.selectionFrame.measureId;
    let newStart = start;
    let newEnd = end;

    if (selectionMeasureId && selectionMeasureId !== measureId) {
      // users are not allowed to copy notes from across measures
      newStart = null;
      newEnd = null;
    } else if (isSingleNote) {
      newStart = chordId;
      newEnd = chordId;
    } else if (chordId < start) {
      if (newEnd == null) {
        newEnd = newStart;
      }
      newStart = chordId;
    } else if (chordId > start && chordId < end) {
      chordId < prevChordId ? (newEnd = chordId) : (newStart = chordId);
    } else {
      newEnd = chordId;
    }

    return update(state, {
      clipboard: {
        selectionFrame: {
          $set: {
            start: newStart,
            end: newEnd,
            measureId,
            prevChordId: chordId
          }
        }
      }
    });
  },
  [SET_FRAME_TO_CLIPBOARD]: (state, action) => {
    const { start, end, measureId } = state.clipboard.selectionFrame;
    console.log("START", start, end, measureId);
    return update(state, {
      clipboard: {
        pastableFrame: {
          $set: {
            start,
            end,
            measureId
          }
        },
        selectionFrame: { $set: dummyFrame }
      }
    });
  },
  undoable: [PASTE_CLIPBOARD_CHORD]
};
