export const SET_CLIPBOARD_CHORD = "SET_CLIPBOARD_CHORD";
export const PASTE_CLIPBOARD_CHORD = "PASTE_CLIPBOARD_CHORD";
export const SET_SELECTION_FRAME_START = "SET_SELECTION_FRAME_START";
export const SET_SELECTION_FRAME = "SET_SELECTION_FRAME";
export const SET_FRAME_TO_CLIPBOARD = "SET_FRAME_TO_CLIPBOARD";
export const PASTE_CLIPBOARD_TO_MEASURE = "PASTE_CLIPBOARD_TO_MEASURE";
