const path = require("path");
import HTMLWebpackPlugin from "html-webpack-plugin";

const HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
  title: "Tabbr",
  template: path.resolve(__dirname, "../../src/templates/index.ejs"),
  rootId: "tabbr",
  inject: "body",
  ogUrl: "tabbr-pachomski.surge.sh",
  ogType: "website",
  ogTitle: "tabbr",
  ogDescription: "Easily create and share guitar and bass tabs for free. No account needed. No BS.",
  ogImage: "",
  filename: path.resolve(__dirname, "../../dist/index.html")
});

export default HTMLWebpackPluginConfig;
